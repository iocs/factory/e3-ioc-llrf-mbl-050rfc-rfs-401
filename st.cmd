require essioc

require sis8300llrf
require llrfsystem

epicsEnvSet("LLRF_P", "MBL-050RFC")
epicsEnvSet("IDX", "401")
epicsEnvSet("LLRF_R", "RFS-LLRF-$(IDX)")
epicsEnvSet("LLRF_DIG_R_1", "RFS-DIG-$(IDX)")
epicsEnvSet("LLRF_RFM_R_1", "RFS-RFM-$(IDX)")
epicsEnvSet("TSELPV", "$(LLRF_P):RFS-EVR-$(IDX):EvtACnt-I.TIME")
epicsEnvSet("RKLY", "RFS-Kly-410:")
epicsEnvSet("RCAV", "EMR-CAV-040")
epicsEnvSet("SEC", "mbl")
epicsEnvSet("RFSTID", "54")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(llrfsystem_DIR)/llrfsystem.iocsh")

iocInit
